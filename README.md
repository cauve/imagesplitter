长图分割工具，将长图分割成A4尺寸比例的多张子图

支持批量分割。

将图片放到当前路径的子目录./imgs下，运行以下命令

```bash
npm install
node split-images.js
```

分割的图片会输出到当前路径的子目录./output下。


