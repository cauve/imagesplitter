const Jimp = require('jimp');

const path = require("path")
const fs = require("fs");

async function cropAndSaveImage(image, x, y, w, h, outputPath) {
  image.crop(x, y, w, h); // crop to the given region
  await image.writeAsync(outputPath).then(() => {
    console.log('write ok:', outputPath);
  });
}

function calA4SplitHeight(width) {
  const a4Ratio = 297 / 210;
  const splitHeight = width * a4Ratio;
  return splitHeight;
}
/**
 * 计算A4比例，split的Y坐标
 * @param {}} width
 * @param {*} height
 * @param {*} splitIndex ，0开始
 */
function getSplitA4Y(width, height, splitIndex) {
  const a4SplitHeight = calA4SplitHeight(width);
  var splitY = a4SplitHeight * splitIndex;
  return splitY <= height ? splitY : height;
}
//下一张图与上一张图加一定的重叠，避免打印后，下一页不知道上一页在哪，或者一行正好被截开的场景
function getSplitA4YWithOverlap(width, height, splitIndex) {
//   //第一张分割不需要重叠
//   if (splitIndex <= 0) {
//     return 0;
//   }
  //3%的重叠
  const a4SplitHeight = calA4SplitHeight(width);
  const overlappedHeight =0.97 * a4SplitHeight;
  var splitY = overlappedHeight * splitIndex;
  return splitY <= height ? splitY : height;
}

/**
 * 一般分割的高度就是h = w * a4Ratio，但最后的长度可能不是正好A4的高度
 * @param {*} width
 * @param {*} height
 * @param {*} splitIndex ，0开始
 */
function getSplitHeight(width, height, y) {
  const a4SplitHeight = calA4SplitHeight(width);
  //一般h = w * a4Ratio，但最后的长度可能不是正好A4的高度
  if (y + a4SplitHeight <= height) {
    return a4SplitHeight;
  } else {
    return height - y;
  }
}

/**
 * 获取一张图片所有的分割位置信息
 */
function getImageSplitPositionArr(imgWidth, imgHeight) {
  var positionArr = [];
  var splitIndex = 0;
  while (true) {
    var x = 0;
    var y = getSplitA4YWithOverlap(imgWidth, imgHeight, splitIndex);
    var w = imgWidth;
    var h = getSplitHeight(imgWidth, imgHeight, y);
    positionArr.push({ x, y, w, h });
    var isThisLastOne = y + h >= imgHeight;
    if (isThisLastOne) {
      console.log('last position:', x, y, w, h);
      return positionArr;
    }else{
        splitIndex++;
    }
  }
}

function getWorkDirFileList(workDir){
    let files = fs.readdirSync(workDir);
    let dirPathList = [];
    let filePathList = [];
    let dirList = [];
    let fileList = [];
    files.forEach((filename, index) => {
        let itemPath = path.join(workDir, filename);
        const stats = fs.statSync(itemPath);
        if (stats.isDirectory()) {
            console.debug("dir:" + filename);
            dirPathList.push(itemPath)
            dirList.push(filename);
        } else {
            console.debug("file:" + filename);
            filePathList.push(itemPath)
            fileList.push(filename)
        }
    })
    return { dirPathList, filePathList, dirList, fileList };
}


async function main(params) {
    const list = getWorkDirFileList("./imgs");
    const imageFilePathList = list.filePathList;

    for (let imgPath of imageFilePathList){  
        await Jimp.read(imgPath)
        .then(async (image) => {
          var imgWidth = image.getWidth();
          var imgHeight = image.getHeight();
          var positionArr = getImageSplitPositionArr(imgWidth, imgHeight);
    
          
          var imgSuffix = path.extname(imgPath);
          var imgBaseName = path.basename(imgPath, imgSuffix);
    
          for (let i = 0; i < positionArr.length; ++i) {
            let position = positionArr[i];
            const outputPath = `./output/${imgBaseName}/${imgBaseName}_${i}${imgSuffix}`;
            await cropAndSaveImage(
              image.clone(),
              position.x,
              position.y,
              position.w,
              position.h,
              outputPath
            );
          }
        })
        .catch((err) => {
          console.error('err', err);
        });
    }
}

main();
